<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */


class Amasty_Finder_Block_Product_View_List extends Mage_Core_Block_Template
{
    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!Mage::getStoreConfig('amfinder/general/show_active_finder_options')) {
            return '';
        }

        $this->setTitle(Mage::getStoreConfig('amfinder/general/name_finder_options_tab'));

        return parent::_toHtml();
    }

    /**
     * @return array
     */
    public function getOptionsList()
    {
        $productId = Mage::registry('product')->getId();
        $dropdowns = $this->getDropdowns();

        $names = array();
        if ($dropdowns) {
            $finderValues = Mage::getModel('amfinder/map')->getCollection()->getDependsValues($productId, $dropdowns);

            foreach ($finderValues as $finderValue) {
                foreach ($finderValue->getData() as $key => $value) {
                    if (strpos($key, 'value_name') !== false) {
                        $names[$finderValue->getId()][] = $value;
                    }
                }
            }
        }

        return $names;
    }

    /**
     * @return array
     */
    public function getDropdowns()
    {
        $session = Mage::getSingleton('catalog/session');

        return $session->getData('finder_dropdowns');
    }
}
