<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */
class Amasty_Finder_Block_Adminhtml_Finder_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    const ALL_CATEGORIES = 0;

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $hlp = Mage::helper('amfinder');
        $model = Mage::registry('amfinder_finder');

        $fldInfo = $form->addFieldset('general', array('legend' => $hlp->__('General')));

        $fldInfo->addField(
            'name',
            'text',
            array(
                'label' => $hlp->__('Title'),
                'name' => 'name',
                'required' => true,
            )
        );

        if (!$model->getId()) {
            $fldInfo->addField(
                'cnt',
                'text',
                array(
                    'label' => $hlp->__('Number of Dropdowns'),
                    'name' => 'cnt',
                    'required' => true,
                    'class' => 'validate-greater-than-zero',
                )
            );
        }

        $fldInfo->addField(
            'template',
            'select',
            array(
                'label' =>  $hlp->__('Template'),
                'name' => 'template',
                'required' => true,
                'values' => array(
                    array('value' => 'horizontal', 'label' => Mage::helper('catalog')->__('horizontal')),
                    array('value' => 'vertical', 'label' => Mage::helper('catalog')->__('vertical'))
                )
            )
        );

        $fldInfo->addField(
            'default_category',
            'select',
            array(
                'label' => $hlp->__('Add Finder to the Default Category'),
                'name' => 'default_category',
                'values' => array(
                    array('value' => 1, 'label' => Mage::helper('catalog')->__('Yes')),
                    array('value' => 0, 'label' => Mage::helper('catalog')->__('No'))
                ),
                'note' => $hlp->__('Keep \'Yes\' to get the Finder working properly at the home and cms pages')
            )
        );

        $fldInfo->addField(
            'hide_finder',
            'select',
            array(
                'label' => $hlp->__('Finder block visibility on the Default Category'),
                'name' => 'hide_finder',
                'values' => array(
                    array('value' => 0, 'label' => Mage::helper('catalog')->__('Show')),
                    array('value' => 1, 'label' => Mage::helper('catalog')->__('Hide'))
                ),
                'note' => $hlp->__(
                    'Keep \'Hide\' to not display this finder block on the default category on the 
                    frontend. It is useful when there are several finders added to the default category but you need to 
                    display only one of them at the frontend.'
                )
            )
        );

        $fldInfo->addField(
            'position',
            'text',
            array(
                'label' => $hlp->__('Add the finder block in'),
                'name' => 'position',
                'note' => $hlp->__(
                    'Place the product finder in particular themes, categories, and other locations. Possible options:
                    left, left_first (rwd theme), right, content, footer.'
                )
            )
        );

        $fldInfo->addField(
            'categories',
            'multiselect',
            array(
                'label' => $hlp->__('Categories'),
                'name' => 'categories',
                'values' => $this->getTree()
            )
        );

        $depend = $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence');
        $depend->addFieldMap('default_category', 'default_category')
            ->addFieldMap('hide_finder', 'hide_finder')
            ->addFieldDependence('hide_finder', 'default_category', 1);

        $this->setChild('form_after', $depend);

        $fldInfo->addField(
            'custom_url',
            'text',
            array(
                'label' => $hlp->__('Custom Destination URL'),
                'name' => 'custom_url',
                'note' => $hlp->__(
                    'E.g. special-category.html  In most cases you don`t need to set it.
                    Useful when you have 2 or more finders and want to show search results at specific categories.
                    It`s NOT the url key.
                    You can modify /amfinder/ url key in app/code/local/Amasty/Finder/etc/config.xml'
                ),
            )
        );
        if (!$model->getId()) {
            $model->setData('default_category', 1);
            $model->setData('hide_finder', 1);
        }
        //set form values
        $form->setValues($model->getData());

        return parent::_prepareForm();
    }

    /**
     * @return array
     */
    public function getTree()
    {
        $rootId = Mage::app()->getStore(0)->getRootCategoryId();
        $tree = array();
        $collection = Mage::getModel('catalog/category')
            ->getCollection()->addNameToResult();

        $pos = array();
        foreach ($collection as $cat) {
            $path = explode('/', $cat->getPath());
            if ((!$rootId || in_array($rootId, $path)) && $cat->getLevel() > 1) {
                $tree[$cat->getId()] = array(
                    'label' => str_repeat('--', $cat->getLevel()) . $cat->getName(),
                    'value' => $cat->getId(),
                    'path'  => $path,
                );
            }
            $pos[$cat->getId()] = $cat->getPosition();
        }

        foreach ($tree as $catId => $cat) {
            $order = array();
            foreach ($cat['path'] as $id) {
                $order[] = isset($pos[$id]) ? $pos[$id] : 0;
            }
            $tree[$catId]['order'] = $order;
        }

        usort($tree, array($this, 'compare'));
        array_unshift($tree, array('value' => self::ALL_CATEGORIES, 'label' => 'All Categories'));

        return $tree;
    }

    /**
     * @param $first
     * @param $second
     * @return int
     */
    public function compare($first, $second)
    {
        foreach ($first['path'] as $i => $id) {
            if (!isset($second['path'][$i])) {
                // B path is shorther then A, and values before were equal
                return 1;
            }
            if ($id != $second['path'][$i]) {
                // compare category positions at the same level
                $p = isset($first['order'][$i]) ? $first['order'][$i] : 0;
                $p2 = isset($second['order'][$i]) ? $second['order'][$i] : 0;

                return ($p < $p2) ? -1 : 1;
            }
        }
        // B path is longer or equal then A, and values before were equal
        return ($first['value'] == $second['value']) ? 0 : -1;
    }
}
