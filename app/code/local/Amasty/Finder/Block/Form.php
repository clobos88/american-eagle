<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */
class Amasty_Finder_Block_Form extends Mage_Core_Block_Template
{
    protected $_finderModel = null;
    protected $_parentDropdownId = 0;
    protected $_finderList = array();

    public function getCacheKey()
    {
        $session = Mage::getSingleton('catalog/session');
        $params = $session->getData('amfinder_' . $this->getId());
        $key = '';
        foreach ($params as $param) {
            $key .= $param;
        }

        return $key;
    }

    /**
     * @return Amasty_Finder_Model_Finder
     */
    public function getFinder()
    {
        if ($this->_finderModel === null) {
            $this->_finderModel = Mage::getModel('amfinder/finder')
                ->load($this->getId());
        }

        return $this->_finderModel;
    }

    public function getDropdownAttributes($dropdown)
    {
        $html = sprintf(
            'name="finder[%s]" id="finder-%s--%s"',
            $dropdown->getId(), $this->getId(), $dropdown->getId()
        );

        $parentValueId = $this->getFinder()->getSavedValue($this->_parentDropdownId);
        $currentValueId = $this->getFinder()->getSavedValue($dropdown->getId());

        if ($this->_isHidden($dropdown) && (!$parentValueId) && (!$currentValueId)) {
            $html .= 'disabled="disabled"';
        }

        return $html;
    }

    public function getDropdownValues($dropdown)
    {
        $values = array();

        $parentValueId = $this->getFinder()->getSavedValue($this->_parentDropdownId);
        $currentValueId = $this->getFinder()->getSavedValue($dropdown->getId());

        if ($this->_isHidden($dropdown) && (!$parentValueId) && (!$currentValueId)) {
            return $values;
        }

        $values = $dropdown->getValues($parentValueId, $currentValueId);

        $this->_parentDropdownId = $dropdown->getId();

        return $values;
    }

    public function isButtonsVisible()
    {
        $cnt = count($this->getFinder()->getDropdowns());

        // we have just 1 dropdown. show thw button
        if (1 == $cnt) {
            return true;
        }

        // at least one value is selected and we allow partial search
        if ($this->getFinder()->getSavedValue('current') && Mage::getStoreConfig('amfinder/general/partial_search')) {
            return true;
        }

        // all values are selected.
        if (($this->getFinder()->getSavedValue('last'))) {
            return true;
        }

        return false;
    }

    public function getAjaxUrl()
    {
        if (Mage::app()->getStore()->isCurrentlySecure()) {
            $url = Mage::getUrl('amfinder/index/options', array('_secure' => true));
        } else {
            $url = Mage::getUrl('amfinder/index/options', array('_secure' => false));
        }
        return $url;
    }

    public function getBackUrl()
    {

        //  no params
        //  category type CMS -> amfinder / amshopby
        //  cms page including homepage -> amfinder / amshopby

        //  with params
        //  amfinder/amshopby page with params -> amfinder / amshopby amshopby with params
        //  normal category page with pagams -> the same category
        //  landing page -> the same landing page
        $securedFlag = false;
        if (Mage::app()->getStore()->isCurrentlySecure()) {
            $securedFlag = true;
        }

        $secured = array('_secure' => $securedFlag);

        $customUrl = $this->getCustomUrl($secured);

        if ($customUrl) {
            return $customUrl;
        }

        $url = Mage::getUrl('amfinder', $secured);
        $isRootCategory = $this->getCurrentCategoryId() == Mage::app()->getStore()->getRootCategoryId();
        $brandId = Mage::app()->getRequest()->getParam('ambrand_id');
        $controllerName = Mage::app()->getFrontController()->getRequest()->getControllerName();
        if ($this->getCurrentCategory()) {
            $url = $this->getCurrentCategoryUrl();
        } elseif ($controllerName == 'result' || ($this->getCurrentCategoryId() && !$isRootCategory)) {
            $url = Mage::getUrl('*/*/*', array('_current' => true, '_use_rewrite' => true));
        } elseif (Mage::helper('ambase')->isModuleActive('Amasty_Brands') && $brandId) {
            $storeId = Mage::app()->getStore()->getId();
            $url = Mage::getModel('ambrands/brand')->setStoreId($storeId)->load($brandId)->getUrl();
        } elseif (Mage::helper('ambase')->isModuleActive('Amasty_Shopby')
            && Mage::getStoreConfig('amshopby/seo/enable_shopby_page')
        ) {
            $url = Mage::getBaseUrl() . Mage::getStoreConfig('amshopby/seo/key');
        }

        return Mage::helper('amfinder')->formatUrl($url);
    }

    public function getCustomUrl($secured)
    {
        $customUrl = $this->getFinder()->getCustomUrl()
            ? $this->getFinder()->getCustomUrl()
            : Mage::getStoreConfig('amfinder/general/custom_category');

        if ($customUrl) {
            $url = Mage::helper('core/url')->getCurrentUrl();

            // from some different url to custom url
            if (false === strpos($url, $customUrl)) {
                $url = Mage::getUrl($customUrl, $secured);
            }

            // in other case just use the current url
            return Mage::helper('amfinder')->formatUrl($url);
        }

        return false;
    }

    public function getActionUrl()
    {
        if (Mage::app()->getStore()->isCurrentlySecure()) {
            $url = Mage::getUrl('amfinder/index/search', array('_secure' => true));
        } else {
            $url = Mage::getUrl('amfinder/index/search', array('_secure' => false));
        }
        return $url;
    }

    public function getResetUrl()
    {
        if (Mage::getStoreConfig('amfinder/general/reset_home') == 'current') {
            return Mage::helper('amfinder')->formatUrl($this->getCurrentCategoryUrl());
        } else {
            return $this->getBackUrl();
        }
    }

    protected function getCurrentCategoryUrl()
    {
        return Mage::helper('core/http')->getHttpReferer() ?: Mage::getUrl();
    }

    protected function _isHidden($dropdown)
    {
        //it's not the first dropdown && value is not selected
        return ($dropdown->getPos() && !$this->getFinder()->getSavedValue($dropdown->getId()));
    }

    protected function _toHtml()
    {
        $this->apply();

        $id = $this->getId();
        if (!$id) {
            return 'Please specify the Parts Finder ID';
        }

        $finder = $this->getFinder();
        if (!$finder->getId()) {
            return 'Please specify an exiting Parts Finder ID';
        }
        if (in_array($id, $this->_finderList)) {
            return false;
        }
        $this->_finderList[] = $id;
        return parent::_toHtml();
    }

    // we need this method to be able to use the finder in the layout updates
    public function apply()
    {
        if ($this->_isApplied) {
            return $this;
        }
        $this->_isApplied = true;

        $this->setFinderTemplate();
        $finder = $this->getFinder();
        $urlParam = $this->getRequest()->getParam('find');

        // XSS disabling
        $filter = array("<", ">");
        $urlParam = str_replace($filter, "|", $urlParam);
        $urlParam = htmlspecialchars($urlParam);

        if ($urlParam) {
            $urlParam = $finder->parseUrlParam($urlParam);
            $current = $finder->getSavedValue('current');

            if ($urlParam && ($current != $urlParam)) { // url has higher priority than session
                $dropdowns = $finder->getDropdownsByCurrent($urlParam);
                $finder->saveFilter($dropdowns);
            }
        }

        $finder->applyFilter();

        if (Mage::app()->getRequest()->getParam('debug')) {
            $session = Mage::getSingleton('catalog/session');
            $name = 'amfinder_' . $this->getId();
            print_r($session->getData($name));
        }

        return $this;
    }

    public function setFinderTemplate()
    {
        $template = $this->getTemplate();
        if (!$template) {
            $template = $this->getFinder()->getTemplate();
            if (!$template) {
                $template = Amasty_Finder_Model_Finder::VERTICAL_TEMPLATE;
            }
            $this->setTemplate('amfinder/' . $template . '.phtml');
        }
    }

    public function getCurrentCategoryId()
    {
        $category = Mage::registry('current_category');
        if ($category) {
            return $category->getId();
        }

        return Mage::app()->getStore()->getRootCategoryId();
    }

    public function isHide()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        $isDefaultCategory = Mage::getModel('catalog/layer')->getCurrentCategory()->getId()
            == Mage::app()->getStore($storeId)->getRootCategoryId();

        return $this->getFinder()->getHideFinder()
            && $isDefaultCategory
            && !Mage::helper('amfinder')->isNotDefaultPage();
    }
}
