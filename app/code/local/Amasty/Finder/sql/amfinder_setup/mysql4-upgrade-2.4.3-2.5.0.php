<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */

$this->startSetup();

$tableName = $this->getTable('amfinder/finder');
$fieldsSql = 'SHOW COLUMNS FROM ' . $tableName;
$cols = $this->getConnection()->fetchCol($fieldsSql);

if (!in_array('default_category', $cols)) {
    $this->run("ALTER TABLE `{$this->getTable('amfinder/finder')}` ADD `default_category` TINYINT(2) NOT NULL");
    $this->run("ALTER TABLE `{$this->getTable('amfinder/finder')}` ADD `hide_finder` TINYINT(2) NOT NULL");
}

$this->endSetup();
