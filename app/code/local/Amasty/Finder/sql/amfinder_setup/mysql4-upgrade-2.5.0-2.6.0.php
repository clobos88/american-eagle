<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */

$this->startSetup();

$tableName = $this->getTable('amfinder/finder');
$fieldsSql = 'SHOW COLUMNS FROM ' . $tableName;
$cols = $this->getConnection()->fetchCol($fieldsSql);

if (!in_array('categories', $cols)) {
    $this->run("ALTER TABLE `{$tableName}` ADD COLUMN `categories` TEXT NOT NULL default '';");
    $this->run("ALTER TABLE `{$tableName}` ADD COLUMN `position` VARCHAR(50) NOT NULL default 'content';");
}

$this->endSetup();
