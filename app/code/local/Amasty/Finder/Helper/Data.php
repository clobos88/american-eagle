<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */
class Amasty_Finder_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SORT_STRING_ASC = 0;
    const SORT_STRING_DESC = 1;
    const SORT_NUM_ASC = 2;
    const SORT_NUM_DESC = 3;

    const FTP_IMPORT_DIR = '/media/amfinder/ftp_import/';

    const FILE_STATE_PROCESSING = 'processing';
    const FILE_STATE_ARCHIVE = 'archive';

    /**
     * @param $url
     * @return string
     */
    public function formatUrl($url)
    {

        if (Mage::app()->getStore()->isCurrentlySecure()) {
            $securedFlag = true;
        } else {
            $securedFlag = false;
        }

        $fpcEnabled = Mage::helper('core')->isModuleEnabled('Enterprise_PageCache');

        if ($fpcEnabled) {
            $url .= strpos($url, '?') ? '&no_cache=1' : '?no_cache=1';
        }

        if ($securedFlag) {
            $url = str_replace("http://", "https://", $url);
        }

        return Mage::helper('core')->urlEncode($url);
    }

    /**
     * @return string
     */
    public function getFtpImportDir()
    {
        return Mage::getBaseDir() . self::FTP_IMPORT_DIR;
    }

    /**
     * @return int
     */
    public function getArchiveLifetime()
    {
        return Mage::getStoreConfig('amfinder/import/archive_lifetime');
    }

    /**
     * @return int
     */
    public function getMaxRowsPerImport()
    {
        return Mage::getStoreConfig('amfinder/import/max_rows_per_import');
    }

    /**
     * @param $finder
     * @return string
     */
    public function getFinderXmlCode($finder)
    {
        $finderPosition = $finder->getPosition() ?: 'content';

        return '<reference name="' . $finderPosition . '">'
            . '<block type ="amfinder/form" name="amfinder' . $finder->getId() . '" before="-"> '
            . '<action method="setId"><id>' . $finder->getId() . '</id></action> '
            . '<action method="apply" /> '
            . '<action method="setLocation"><location>xml</location></action> '
            . '</block> '
            . '</reference>';
    }

    /**
     * @return bool
     */
    public function isNotDefaultPage()
    {
        return Mage::registry('current_product')
            || Mage::getBlockSingleton('page/html_header')->getIsHomePage()
            || Mage::app()->getRequest()->getRouteName() === 'cms'
            || Mage::app()->getRequest()->getRouteName() === 'checkout'
            || Mage::app()->getRequest()->getRouteName() === 'contacts';
    }
}
