<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */


class Amasty_Finder_Model_Mysql4_Map_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('amfinder/map');
    }

    /**
     * @param $productId
     * @return $this
     */
    public function getDependsValues($productId, $dropdowns)
    {
        $dropdownsCount = count($dropdowns);

        $select = $this->getSelect();

        $select->joinInner(
            array('amfinder_value_0' => $this->getTable('amfinder/value')),
            'amfinder_value_0.value_id = main_table.value_id'
        );

        $columns['value_name'] = 'amfinder_value_0.name';
        for ($i = 1; $i < $dropdownsCount; $i++) {
            $select->joinInner(
                array('amfinder_value_' . $i => $this->getTable('amfinder/value')),
                'amfinder_value_' . $i . '.value_id = amfinder_value_' . ($i - 1) . '.parent_id'
            );
            $columns['value_name' . $i] = 'amfinder_value_' . $i . '.name';
        }

        $select->where('pid = ?', $productId);
        $select->where('amfinder_value_' . ($i - 1) . '.dropdown_id IN (' . implode(",", array_keys($dropdowns)) . ')');
        $select->columns($columns);


        return $this;
    }
}
