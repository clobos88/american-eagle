<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */


class Amasty_Finder_Model_Mysql4_Map extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('amfinder/map', 'id');
    }
}
