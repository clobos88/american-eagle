<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */
class Amasty_Finder_Model_Mysql4_Finder_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('amfinder/finder');
    }

    /**
     * @param $categoryId
     * @return $this
     */
    public function addCategoryFilter($categoryId)
    {
        $this->addFieldToFilter(
            'categories',
            array(
                array('categories', 'like' => '%,' . $categoryId . ',%'),
                array('categories', 'like' => '%,' .
                    Amasty_Finder_Block_Adminhtml_Finder_Edit_Tab_General::ALL_CATEGORIES . ',%'))
        );

        return $this;
    }
}
