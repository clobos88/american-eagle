<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */


class Amasty_Finder_Model_Observer
{
    /**
     * @var int
     */
    protected $_cmsId;

    /**
     * @param $observer
     * @return $this
     */
    public function onCoreBlockAbstractToHtmlBefore($observer)
    {
        if ($observer->getBlock() instanceof Amasty_Finder_Block_Form) {
            $observer->getBlock()->setCmsId($this->_cmsId);
        }

        if ($observer->getBlock() instanceof Mage_Cms_Block_Block) {
            $blockModel = Mage::getModel('cms/block')->load($observer->getBlock()->getBlockId());
            if (strpos($blockModel->getContent(), 'amfinder/form') !== false) {
                $this->_cmsId = $observer->getBlock()->getBlockId();
                $observer->getBlock()->setCacheLifetime(null);
            }
        }
        return $this;
    }

    /**
     * @param $observer
     * @return $this
     */
    public function onLayoutGenerateBefore($observer)
    {
        if (Mage::helper('amfinder')->isNotDefaultPage()) {
            return $this;
        }

        $currentCategoryId = Mage::getModel('catalog/layer')->getCurrentCategory()->getId();
        $finderOnCategory = null;
        if ($currentCategoryId == Mage::app()->getStore()->getRootCategoryId()) {
            $finderOnCategory = Mage::getModel('amfinder/finder')->getCollection()
                ->addFieldToFilter('default_category', 1);
        } else {
            $finderOnCategory = Mage::getModel('amfinder/finder')->getCollection()
                ->addCategoryFilter($currentCategoryId);
        }

        if ($finderOnCategory) {
            foreach ($finderOnCategory as $finder) {
                $observer->getLayout()->getUpdate()
                    ->addUpdate(Mage::helper('amfinder')->getFinderXmlCode($finder));
            }
        }

        return $this;
    }
}
