<?php

class Aod_Ymm_SearchController extends Mage_Core_Controller_Front_Action
{
    /**
     * @var Aod_Ymm_Helper_Data
     */
    protected $helper;

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        $this->helper = Mage::helper('ymm');
    }

    public function indexAction()
    {
        // Nothing
    }

    private function sendJson($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function get_yearsAction()
    {
        $this->sendJson([
            'items' => $this->helper->getAttributeValues($this->helper->getYearKey())
        ]);
    }

    public function get_makesAction()
    {
        $this->sendJson([
            'items' => $this->helper->getAttributeValues($this->helper->getMakeKey(), [
                $this->helper->getYearKey() => $this->getRequest()->getParam($this->helper->getYearKey()),
            ])
        ]);
    }

    public function get_modelsAction()
    {
        $this->sendJson([
            'items' => $this->helper->getAttributeValues($this->helper->getModelKey(), [
                $this->helper->getYearKey() => $this->getRequest()->getParam($this->helper->getYearKey()),
                $this->helper->getMakeKey() => $this->getRequest()->getParam($this->helper->getMakeKey()),
            ])
        ]);
    }

    public function removeAction()
    {
        $redirect = $this->getRequest()->getParam('redirect', Mage::getBaseUrl());

        $_SESSION[$this->helper->getSavedKey()] = [
            $this->helper->getYearKey()  => null,
            $this->helper->getMakeKey()  => null,
            $this->helper->getModelKey() => null
        ];

        $this->getResponse()->setRedirect($redirect)->sendResponse();
    }
} 
