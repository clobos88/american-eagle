<?php

class Aod_Ymm_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ENTITY_ATTRIBUTE_YEAR = 'year';
    const ENTITY_ATTRIBUTE_MAKE = 'make';
    const ENTITY_ATTRIBUTE_MODEL = 'v_model';
    const SESSION_SAVED_KEY = '_selected_vehicle';

    /**
     * Returns the session save key
     * @return string
     */
    public function getSavedKey()
    {
        return self::SESSION_SAVED_KEY;
    }

    /**
     * Returns the vehicle year key
     * @return string
     */
    public function getYearKey()
    {
        return self::ENTITY_ATTRIBUTE_YEAR;
    }

    /**
     * Returns the vehicle make key
     * @return string
     */
    public function getMakeKey()
    {
        return self::ENTITY_ATTRIBUTE_MAKE;
    }

    /**
     * Returns the vehicle model key
     * @return string
     */
    public function getModelKey()
    {
        return self::ENTITY_ATTRIBUTE_MODEL;
    }

    /**
     * Returns the unique values stored in the entity table based on different combinations
     *
     * @param string $attribute_name
     * @param array $attribute_joins
     * @param string $order
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    public function getAttributeValues($attribute_name = '', $attribute_joins = [], $order = 'ASC')
    {
        /**
         * @var Mage_Core_Model_Resource $resource
         */
        $resource = Mage::getSingleton('core/resource');
        $db = $resource->getConnection(Mage_Core_Model_Resource::DEFAULT_READ_RESOURCE);
        $attributes = $resource->getTableName('eav_attribute');
        $entities = $resource->getTableName('catalog_product_entity_varchar');

        $attr_ids = [];
        foreach (array_merge([$attribute_name], array_keys($attribute_joins)) as $attribute_key) {
            $row = $db->query("SELECT attribute_id FROM {$attributes} WHERE attribute_code='{$attribute_key}'")->fetch();
            $attr_ids[$attribute_key] = $row['attribute_id'];
        }

        $sql = "SELECT DISTINCT value FROM {$entities} WHERE attribute_id={$attr_ids[$attribute_name]}";
        unset($attr_ids[$attribute_name]);

        if (count($attr_ids) > 0) {
            $joins = [];
            foreach ($attr_ids as $name => $id) {
                $join_sql = "SELECT entity_id FROM {$entities} WHERE attribute_id={$id} AND value like '%{$attribute_joins[$name]}%'";

                $joins[] = $join_sql;
            }

            $sql .= ' AND entity_id IN (';
            $sql .= implode(' AND entity_id IN (', $joins);
            $sql .= str_repeat(')', count($attr_ids));
        }

        $sql .= ' ORDER BY value ' . $order;
        $rows = $db->query($sql)->fetchAll();

        return array_map(function ($item) {
            return $item['value'];
        }, $rows);
    }
}
