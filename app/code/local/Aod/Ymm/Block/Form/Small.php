<?php

class Aod_Ymm_Block_Form_Small extends Mage_Core_Block_Template
{
    /**
     * @var Aod_Ymm_Helper_Data
     */
    protected $helper;

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        $this->helper = Mage::helper('ymm');
        $this->setSessionKeys();
    }

    /**
     * Sets the session stored vehicle to an empty one
     */
    private function setSessionKeys() {
        if(!isset($_SESSION[$this->helper->getSavedKey()])) {
            $_SESSION[$this->helper->getSavedKey()] = [
                $this->helper->getYearKey()  => null,
                $this->helper->getMakeKey()  => null,
                $this->helper->getModelKey() => null
            ];
        }
    }

    /**
     * Returns the search form url
     * @return string
     */
    public function getSearchFormUrl()
    {
        return Mage::getUrl('catalogsearch/advanced/result');
    }

    public function getCurrentUrl()
    {
        /**
         * @var Mage_Core_Helper_Url $helper
         */
        $helper = Mage::helper('core/url');
        $current = htmlspecialchars_decode($helper->getCurrentUrl());
        $current = explode('?', $current);

        parse_str(array_pop($current), $vars);

        foreach([
            $this->helper->getYearKey(),
            $this->helper->getMakeKey(),
            $this->helper->getModelKey(),
        ] as $key) {
            if(isset($vars[$key])) {
                unset($vars[$key]);
            }
        }

        $vars = http_build_query($vars);

        return array_shift($current) . (!empty($vars) ? '?' . $vars : '') ;
    }

    /**
     * Returns an array of vehicle years
     * @return array
     */
    public function getYears()
    {
        return $this->helper->getAttributeValues($this->helper->getYearKey(), [], 'DESC');
    }

    /**
     * Checks to see if the current request has a vehicle
     * @return bool
     */
    public function hasSelectedVehicle()
    {
        foreach(array_keys($_SESSION[$this->helper->getSavedKey()]) as $key) {
            if(isset($_REQUEST[$key])) {
                $_SESSION[$this->helper->getSavedKey()][$key] = $_REQUEST[$key];
            }
        }

        return count(array_filter($_SESSION[$this->helper->getSavedKey()])) > 0;
    }

    /**
     * Returns the vehicle information stored in session
     * @return array
     */
    public function getSelectedVehicle()
    {
        return $_SESSION[$this->helper->getSavedKey()];
    }

    /**
     * Returns the stored vehicle year
     * @return string
     */
    public function getVehicleYear()
    {
        return ucwords($this->getSelectedVehicle()[$this->helper->getYearKey()]);
    }

    /**
     * Returns the stored vehicle make
     * @return string
     */
    public function getVehicleMake()
    {
        return ucwords($this->getSelectedVehicle()[$this->helper->getMakeKey()]);
    }

    /**
     * Returns the stored vehicle model
     * @return string
     */
    public function getVehicleModel()
    {
        return ucwords($this->getSelectedVehicle()[$this->helper->getModelKey()]);
    }
}
