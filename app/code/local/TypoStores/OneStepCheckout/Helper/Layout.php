<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
class TypoStores_OneStepCheckout_Helper_Layout extends Mage_Checkout_Helper_Data{

    public function switchTemplate(){
        $layout = Mage::getStoreConfig('typostores_onestepcheckout/layout/layout');
        switch ($layout){
            case '2cols':
                return 'typostores/onestepcheckout/onepage.phtml';
                break;
            case '3cols':
                return 'typostores/onestepcheckout/onepage_3columns.phtml';
                break;
            default:
                return 'typostores/onestepcheckout/onepage.phtml';
                break;
        }
    }
}