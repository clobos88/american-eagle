<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
class TypoStores_OneStepCheckout_Block_Checkout_Links extends  Mage_Checkout_Block_Links {

    public function addCheckoutLink()
    {

        if (!Mage::getStoreConfig('typostores_onestepcheckout/general/enabled')) {
            return parent::addCheckoutLink();
        }


        $parentBlock = $this->getParentBlock();
        if ($parentBlock && Mage::helper('core')->isModuleOutputEnabled('TypoStores_OneStepCheckout')) {
            $text = $this->__('Checkout');
            $parentBlock->addLink(
                $text, 'typostores_onestepcheckout', $text,
                true, array('_secure' => true), 60, null,
                'class="top-link-checkout"'
            );
        }
        return $this;
    }
}