<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_TypoConfig_Block_Adminhtml_Cfgporter_Import_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->_blockGroup = 'typoconfig';
		$this->_controller = 'adminhtml_cfgporter_import';
		$this->_headerText = Mage::helper('typoconfig')->__('Import Configuration');

		$this->_updateButton('save', 'label', Mage::helper('typoconfig')->__('Import Configuration'));
		$this->_updateButton('save', 'style', 'background-image:none; border-color:#BE1840; background-color:#EE1448;');

		$this->_removeButton('back');
		$this->_updateButton('reset', 'label', Mage::helper('typoconfig')->__('Reset Form'));
	}
}
