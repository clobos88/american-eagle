<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_TypoConfig_Model_Source_Cfgporter_Packagepresets
{
	protected $_options;

	public function toOptionArray($package = NULL)
	{
		if (!$this->_options)
		{
			$this->_options = array();
			$this->_options[] = array('value' => '', 'label' => Mage::helper('typoconfig')->__('-- Please Select --')); //First option is empty

			$dir = Mage::helper('typoconfig/cfgporter_data')->getPresetDir($package);
			if (is_dir($dir))
			{
				$files = scandir($dir);
				foreach ($files as $file)
				{
					if (!is_dir($dir . $file))
					{
						$path = pathinfo($file);
						$this->_options[] = array('value' => $path['filename'], 'label' => $path['filename']);
					}
				}
			}

			//Last option
			$this->_options[] = array('value' => 'upload_custom_file', 'label' => Mage::helper('typoconfig')->__('Upload custom file...'));
		}
		return $this->_options;
	}
}
