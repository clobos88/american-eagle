<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_TypoConfig_Model_Source_Cfgporter_Packagemodules
{
	protected $_options;

	public function toOptionArray($package = NULL)
	{
		if (!$this->_options)
		{
			$this->_options = array();
			$this->_options[] = array('value' => '', 'label' => Mage::helper('typoconfig')->__('-- Please Select --')); //First option is empty

			if (NULL !== $package)
			{
				$h = Mage::helper('typoconfig/cfgporter_data');
				$modules = $h->getPackageModules($package);
				if ($modules)
				{
					$moduleNames = $h->getModuleNames();
					foreach ($modules as $mod)
					{
						$this->_options[] = array('value' => $mod, 'label' => $moduleNames[$mod]);
					}
				}
			}
			else
			{
				$modulesFromConfig = (array) Mage::getConfig()->getNode('modules')->children();
				$modules = array_keys($modulesFromConfig);
				sort($modules);
				foreach ($modules as $mod)
				{
					$this->_options[] = array('value' => $mod, 'label' => $mod);
				}
			}
		}
		return $this->_options;
	}
}
