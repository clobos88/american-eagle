<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_TypoConfig_Helper_Cfgporter_Data extends Mage_Core_Helper_Abstract
{
	const PRESET_FILE_MAIN_DIR		= 'config';

	/**
	 * Modules associated with package
	 *
	 * @var array
	 */
	protected $_packageModules = array(
		'TypoStores_TypoLogancee' =>
			array('TypoStores_TypoLogancee', 'TypoStores_AjaxCart', 'TypoStores_CategorySearch', 'TypoStores_Filter', 'TypoStores_Newsletter', 'TypoStores_Instagram', 'AW_Blog'),
	);

	/**
	 * Human-readable names of modules
	 *
	 * @var array
	 */
	protected $_moduleNames = array(
		'TypoStores_TypoLogancee'		=> 'Typo Logancee',
		'TypoStores_AjaxCart'			=> 'Typo AjaxCart',
		'TypoStores_CategorySearch'		=> 'Typo Category Search',
		'TypoStores_Filter'				=> 'Typo Filter',
		'TypoStores_Newsletter'			=> 'Typo Newsletter Popup',
		'TypoStores_Instagram'			=> 'Typo Instagram Feed',
		'AW_Blog'						=> 'AW Blog',
	);

	/**
	 * File path elements
	 *
	 * @var string
	 */
	protected $_presetFileExt = 'xml';
	protected $_presetFileModuleDirType = 'etc';
	protected $_presetFileTmpBaseDir;

	/**
	 * Resource initialization
	 */
	public function __construct()
	{
		$this->_presetFileTmpBaseDir = Mage::getBaseDir('export');
	}

	/**
	 * Get modules associated with package
	 *
	 * @param string
	 * @return array
	 */
	public function getPackageModules($package)
	{
		if (isset($this->_packageModules[$package]))
		{
			return $this->_packageModules[$package];
		}
	}

	/**
	 * Get human-readable names of modules
	 *
	 * @return array
	 */
	public function getModuleNames()
	{
		return $this->_moduleNames;
	}

	/**
	 * Get human-readable name of a module
	 *
	 * @param string
	 * @return string
	 */
	public function getModuleName($module)
	{
		if (isset($this->_moduleNames[$module]))
		{
			return $this->_moduleNames[$module];
		}
	}

	/**
	 * Determines and returns file path of the config preset file
	 *
	 * @param string
	 * @param string
	 * @return string
	 */
	public function getPresetFilepath($filename, $modulename)
	{
		$baseDir = $this->getPresetDir($modulename);
		return $baseDir . DS . $filename . '.' . $this->_presetFileExt;
	}

	/**
	 * Determines and returns path of the directory with config preset files
	 *
	 * @param string
	 * @return string
	 */
	public function getPresetDir($modulename)
	{
		if ($modulename)
		{
			$baseDir = Mage::getConfig()->getModuleDir($this->_presetFileModuleDirType, $modulename);
		}
		else
		{
			$baseDir = $this->_presetFileTmpBaseDir . DS . TypoStores_TypoConfig_Helper_Data::FILE_TOP_LEVEL_DIR;
		}

		return $baseDir . DS . TypoStores_TypoConfig_Helper_Data::FILE_MAIN_DIR . DS . self::PRESET_FILE_MAIN_DIR;
	}
}
