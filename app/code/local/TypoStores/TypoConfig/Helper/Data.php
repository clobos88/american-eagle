<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_TypoConfig_Helper_Data extends Mage_Core_Helper_Abstract
{
	const FILE_TOP_LEVEL_DIR	= 'typoconfig';
	const FILE_MAIN_DIR			= 'importexport';

	/**
	 * File path elements
	 *
	 * @var string
	 */
	protected $_tmpFileBaseDir; //Desitnation directory for files uploaded via form

	/**
	 * Resource initialization
	 */
	public function __construct()
	{
		$this->_tmpFileBaseDir = Mage::getBaseDir('media') . DS . 'tmp' . DS . self::FILE_TOP_LEVEL_DIR . DS;
	}

	/**
	 * Get desitnation directory for files uploaded via form
	 *
	 * @return string
	 */
	public function getTmpFileBaseDir()
	{
		return $this->_tmpFileBaseDir;
	}

}
