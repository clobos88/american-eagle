<?php 
class TypoStores_AjaxSwatches_AjaxController extends Mage_Core_Controller_Front_Action {

public function updateAction(){


if(!isset($_REQUEST['pid'])) { exit; }

$pid = $_REQUEST['pid'];

$_product = Mage::getModel('catalog/product')->load($pid);
//get Product


$mediaImages= $_product->getMediaGalleryImages();

$images = array();
$i=1;

$theme = Mage::helper('typologancee');
$imgWidth = $theme->getCfg('category/image_width') ? $theme->getCfg('category/image_width') : 270;
$imgHeight = $theme->getCfg('category/image_height') ? $theme->getCfg('category/image_height') : 360;
if($theme->getCfg('category/aspect_ratio')){
    $imgHeight = 0;
}

foreach ($mediaImages as $_image){
    //var_dump($image);
    if(!$_image['disabled_default']){
        
        //echo Mage::helper('catalog/image')->init($_product, 'thumbnail', $_image->getFile())->resize(75);
        $newImage = array(
            'thumb' => (string) Mage::helper('catalog/image')->init($_product, 'thumbnail', $_image->getFile())->resize(135,round(135*$imgHeight/$imgWidth)),
            'image' => (string) Mage::helper('catalog/image')->init($_product, 'image', $_image->getFile()),
            'image-thumb' => (string) Mage::helper('catalog/image')->init($_product, 'image', $_image->getFile())->resize(540,round(540*$imgHeight/$imgWidth))
        );
        
        
        $images[$i] = $newImage;
        $i++;
    } else {
        //echo 'image disabled';
    }
    
}


$this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($images));
return; 

//return $images;

}

public function getlistdataAction(){

if(!isset($_REQUEST['pids'])) { exit; }

if (!Mage::helper('configurableswatches')->isEnabled()) { // check if functionality disabled
    exit; // exit without loading swatch functionality
}

$pids = explode(',',$_REQUEST['pids']);

$response = $swatches = $jsons = array();
$this->loadLayout();

$viewMode = (isset($_REQUEST['viewMode']))? $_REQUEST['viewMode'] : 'grid';
$keepFrame = ($viewMode == 'grid')? true : false;

foreach($pids as $pid){
    
    Mage::unregister('catViewKeepFrame');
    Mage::register('catViewKeepFrame',$keepFrame);
        
        
    $swatches[] = array('id' => $pid, 'value' => $this->getLayout()
                ->createBlock('TypoStores_AjaxSwatches/swatchlist','swatchlist-'.$pid)
                ->setPid($pid)
                ->setViewMode($viewMode)
                ->setTemplate('configurableswatches/catalog/product/list/swatches.phtml')
                ->toHtml());
    
    $productsCollection = $this->getLayout()->getBlock('swatchlist-'.$pid)->getCollection();
    
    //Mage::log($productsCollection);
    
    $jsons[$pid] = $this->getLayout()
                ->createBlock('TypoStores_AjaxSwatches/catalog_media_js_list','mediajslist-'.$pid)
                ->setPid($pid)
                ->setViewMode($viewMode)
                ->setProductCollection($productsCollection)
                ->setTemplate('typostores/ajaxswatches/media/js.phtml')
                ->toHtml();
                    
}
        
        $response['swatches'] = $swatches;
        
        $response['jsons'] = $jsons;
        
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return; 
}

}