<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
?>
<?php
class TypoStores_TypoLogancee_Adminhtml_Typologancee_ImportController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction() {
        $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/typologancee/", Mage::helper('typologancee')->checkSSL()));
    }
    public function blocksAction() {
        $config = Mage::helper('typologancee')->getCfg('install/overwrite_blocks');
        Mage::getSingleton('typologancee/import_cms')->importCmsItems('cms/block', 'blocks', $config);
        $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/typologancee/", Mage::helper('typologancee')->checkSSL()));
    }
    public function pagesAction() {
        $config = Mage::helper('typologancee')->getCfg('install/overwrite_pages');
        Mage::getSingleton('typologancee/import_cms')->importCmsItems('cms/page', 'pages', $config);
        $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/typologancee/", Mage::helper('typologancee')->checkSSL()));
    }
    public function widgetsAction() {
        Mage::getSingleton('typologancee/import_cms')->importWidgetItems('widget/widget_instance', 'widgets', false);
        $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/typologancee/", Mage::helper('typologancee')->checkSSL()));
    }
}
