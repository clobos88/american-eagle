<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
?>
<?php
class TypoStores_TypoLogancee_Model_System_Config_Source_Layout_Header_Header
{

    public function toOptionArray()
    {
        return array(
            array('value' => 'layout1', 'label' => Mage::helper('adminhtml')->__('Layout 1')),
            array('value' => 'layout2', 'label' => Mage::helper('adminhtml')->__('Layout 2')),
            array('value' => 'layout3', 'label' => Mage::helper('adminhtml')->__('Layout 3')),
            array('value' => 'layout4', 'label' => Mage::helper('adminhtml')->__('Layout 4')),
            array('value' => 'layout5', 'label' => Mage::helper('adminhtml')->__('Layout 5')),
            array('value' => 'layout6', 'label' => Mage::helper('adminhtml')->__('Layout 6')),
            array('value' => 'layout7', 'label' => Mage::helper('adminhtml')->__('Layout 7')),
            array('value' => 'layout8', 'label' => Mage::helper('adminhtml')->__('Layout 8')),
            array('value' => 'sb-layout1', 'label' => Mage::helper('adminhtml')->__('Light Sidebar')),
            array('value' => 'sb-layout2', 'label' => Mage::helper('adminhtml')->__('Dark Sidebar'))
        );
    }

}
