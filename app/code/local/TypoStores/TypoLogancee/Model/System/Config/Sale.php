<?php
class TypoStores_TypoLogancee_Model_System_Config_Sale
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'sale', 'label'=>Mage::helper('adminhtml')->__('Sale')),
            array('value' => 'percent', 'label'=>Mage::helper('adminhtml')->__('%')),
            array('value' => '', 'label'=>Mage::helper('adminhtml')->__('No')),
        );
    }
}