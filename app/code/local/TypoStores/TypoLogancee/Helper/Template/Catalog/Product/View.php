<?php

class TypoStores_TypoLogancee_Helper_Template_Catalog_Product_View extends Mage_Core_Helper_Abstract
{

    public function getCmsBlockTitle($id)
    {
        return Mage::getModel('cms/block')->setStoreId(Mage::app()->getStore()->getId())->load($id)->getTitle();
    }

}
