<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_Widget_CollectionController extends Mage_Core_Controller_Front_Action{
    public function getAction(){
        //if (!$this->_validateFormKey()) return null;
        if (!$this->getRequest()->isPost()) return null;

        $type   = $this->getRequest()->getPost('type');
        $value  = $this->getRequest()->getPost('value');

        if (!$type && !$value) return null;

        $limit                  = $this->getRequest()->getPost('limit', 10);
        $carousel               = $this->getRequest()->getPost('carousel', 0);
        $column                 = $this->getRequest()->getPost('column', 4);
        $column_count           = $this->getRequest()->getPost('column_count', 4);
        $column_count_1600      = $this->getRequest()->getPost('column_count_1600', 4);
        $column_count_1200      = $this->getRequest()->getPost('column_count_1200', 3);
        $column_count_992       = $this->getRequest()->getPost('column_count_992', 3);
        $column_count_768       = $this->getRequest()->getPost('column_count_768', 2);
        $row                    = $this->getRequest()->getPost('row', 1);
        $pricePrefixId          = $this->getRequest()->getPost('pricePrefixId');
        $cid                    = $this->getRequest()->getPost('cid');
        $template               = $this->getRequest()->getPost('template', 'typostores/widget/collection.phtml');

        /* @var $helper TypoStores_Widget_Helper_Data */
        $helper = Mage::helper('typowidget');
        /* @var $block TypoStores_Widget_Block_Widget_Collection */
        $block = $this->getLayout()->createBlock('typowidget/widget_collection');
        $params = array();

        if ($cid){
            $params['category_ids'] = explode(',', $cid);
        }

        $block->setTemplate($template);
        $block->setData(array(
            'row'                   => $row,
            'column'                => $column,
            'column_count'          => $column_count,
            'column_count_1600'     => $column_count_1600,
            'column_count_1200'     => $column_count_1200,
            'column_count_992'      => $column_count_992,
            'column_count_768'      => $column_count_768,
            'carousel'              => $carousel,
            'collection'            => $helper->getProductCollection($type, $value, $params, $limit),
            'price-prefix'          => $pricePrefixId
        ));

        return $this->getResponse()->setBody($block->toHtml());
    }
}
