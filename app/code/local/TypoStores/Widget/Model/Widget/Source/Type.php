<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_Widget_Model_Widget_Source_Type{
    public function toOptionArray(){
        $types = array(
            array('value' => 'product', 'label' => Mage::helper('typowidget')->__('Product')),
            array('value' => 'block', 'label' => Mage::helper('typowidget')->__('Block')),
            array('value' => 'attribute', 'label' => Mage::helper('typowidget')->__('Attribute')),
            array('value' => 'category', 'label' => Mage::helper('typowidget')->__('Category')),
            array('value' => 'blog', 'label' => Mage::helper('typowidget')->__('Blog'))
        );

        return $types;
    }
}
