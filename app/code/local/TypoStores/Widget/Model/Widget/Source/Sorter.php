<?php

class TypoStores_Widget_Model_Widget_Source_Sorter
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => Varien_Data_Collection::SORT_ORDER_DESC,
                'label' => Mage::helper('typowidget')->__('Newest first'),
            ),
            array(
                'value' => TypoStores_Widget_Helper_Data::BLOG_POST_ORDER_RANDOM,
                'label' => Mage::helper('typowidget')->__('Random'),
            ),
        );
    }
}