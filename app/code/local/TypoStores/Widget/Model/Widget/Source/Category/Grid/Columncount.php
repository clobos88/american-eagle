<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
?>
<?php
class TypoStores_Widget_Model_Widget_Source_Category_Grid_Columncount
{

    public function toOptionArray()
    {
        return array(
            array('value'=>'1', 'label'=>Mage::helper('adminhtml')->__('1')),
            array('value'=>'2', 'label'=>Mage::helper('adminhtml')->__('2')),
            array('value'=>'3', 'label'=>Mage::helper('adminhtml')->__('3')),
            array('value'=>'4', 'label'=>Mage::helper('adminhtml')->__('4')),
            array('value'=>'5', 'label'=>Mage::helper('adminhtml')->__('5')),
            array('value'=>'6', 'label'=>Mage::helper('adminhtml')->__('6'))
        );
    }

}