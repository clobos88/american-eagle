<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_Widget_Model_Widget_Source_Parallax_Overlay{
    public function toOptionArray(){
        $types[] = array('value' => 'none', 'label' => Mage::helper('typowidget')->__('None'));
        $types[] = array('value' => 'typostores/widget/images/gridtile.png', 'label' => Mage::helper('typowidget')->__('2 x 2 Black'));
        $types[] = array('value' => 'typostores/widget/images/gridtile_white.png', 'label' => Mage::helper('typowidget')->__('2 x 2 White'));
        $types[] = array('value' => 'typostores/widget/images/gridtile_3x3.png', 'label' => Mage::helper('typowidget')->__('3 x 3 Black'));
        $types[] = array('value' => 'typostores/widget/images/gridtile_3x3_white.png', 'label' => Mage::helper('typowidget')->__('3 x 3 White'));

        return $types;
    }
}
