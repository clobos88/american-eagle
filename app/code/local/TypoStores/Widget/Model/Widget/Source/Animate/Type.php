<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_Widget_Model_Widget_Source_Animate_Type{
    public function toOptionArray(){
        return array(
            array('value' => 'animate-zoomOut', 'label' => Mage::helper('typowidget')->__('Zoom Out')),
            array('value' => 'animate-zoomIn', 'label' => Mage::helper('typowidget')->__('Zoom In')),
            array('value' => 'animate-bounceIn', 'label' => Mage::helper('typowidget')->__('Bounce In')),
            array('value' => 'animate-bounceInRight', 'label' => Mage::helper('typowidget')->__('Bounce In Right')),
            array('value' => 'animate-pageTop', 'label' => Mage::helper('typowidget')->__('Page Top')),
            array('value' => 'animate-pageBottom', 'label' => Mage::helper('typowidget')->__('Page Bottom')),
            array('value' => 'animate-starwars', 'label' => Mage::helper('typowidget')->__('Star Wars')),
            array('value' => 'animate-pageLeft', 'label' => Mage::helper('typowidget')->__('Page Left')),
            array('value' => 'animate-pageRight', 'label' => Mage::helper('typowidget')->__('Page Right')),
            array('value' => 'animate-fadeIn', 'label' => Mage::helper('typowidget')->__('Fade In')),
            array('value' => 'animate-fadeInDown', 'label' => Mage::helper('typowidget')->__('Fade In Down')),
            array('value' => 'animate-fadeInUp', 'label' => Mage::helper('typowidget')->__('Fade In Up')),
            array('value' => 'animate-flipInX', 'label' => Mage::helper('typowidget')->__('Flip In X')),
            array('value' => 'animate-flipInY', 'label' => Mage::helper('typowidget')->__('Flip In Y'))
        );
    }
}
