<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
class TypoStores_Widget_Model_Widget_Source_Mode{
    public function toOptionArray(){
        $modes = array(
            array('value' => 'latest', 'label' => Mage::helper('typowidget')->__('Latest Products')),
            array('value' => 'new', 'label' => Mage::helper('typowidget')->__('New Products')),
            array('value' => 'bestsell', 'label' => Mage::helper('typowidget')->__('Best Sell Products')),
            array('value' => 'mostviewed', 'label' => Mage::helper('typowidget')->__('Most Viewed Products')),
            array('value' => 'id', 'label' => Mage::helper('typowidget')->__('Specified Products')),
            array('value' => 'random', 'label' => Mage::helper('typowidget')->__('Random Products')),
            array('value' => 'related', 'label' => Mage::helper('typowidget')->__('Related Products')),
            array('value' => 'upsell', 'label' => Mage::helper('typowidget')->__('Up-sell Products')),
            array('value' => 'crosssell', 'label' => Mage::helper('typowidget')->__('Cross-sell Products')),
            array('value' => 'discount', 'label' => Mage::helper('typowidget')->__('Discount Products')),
            array('value' => 'recentviewed', 'label' => Mage::helper('typowidget')->__('Recent Viewed Products')),
            array('value' => 'rating', 'label' => Mage::helper('typowidget')->__('Top Rated Products'))
        );

        return $modes;
    }
}
