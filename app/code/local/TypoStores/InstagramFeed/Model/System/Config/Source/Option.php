<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
class TypoStores_InstagramFeed_Model_System_Config_Source_Option {

    public function toOptionArray() {
        return array(
            array('value' => 'user','label' => Mage::helper('typostoresinstagram')->__('User Id')),
            array('value' => 'tagged','label' => Mage::helper('typostoresinstagram')->__('Hash tag'))
        );
    }
}