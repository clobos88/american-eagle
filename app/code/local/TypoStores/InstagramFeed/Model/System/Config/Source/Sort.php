<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
class TypoStores_InstagramFeed_Model_System_Config_Source_Sort {

    public function toOptionArray() {
        return array(
            array('value' => 'most-recent','label' => Mage::helper('typostoresinstagram')->__('Newest to Oldest')),
            array('value' => 'least-recent','label' => Mage::helper('typostoresinstagram')->__('Oldest to Newest')),
            array('value' => 'most-liked','label' => Mage::helper('typostoresinstagram')->__('Most liked first')),
            array('value' => 'most-commented','label' => Mage::helper('typostoresinstagram')->__('Most commented first')),
            array('value' => 'random', 'label' => Mage::helper('typostoresinstagram')->__('Random'))
        );
    }
}