<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */

class TypoStores_InstagramFeed_Block_Instagram extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface {

    protected function _prepareLayout() {
        if ($head = $this->getLayout()->getBlock('head')) {
            $head->addItem('skin_css','typostores/instagram.css');
        }
        return parent::_prepareLayout();
    }

    protected function _beforeToHtml(){
        $this->setTemplate('typostores/instagramfeed/feed.phtml');
        return parent::_beforeToHtml();
    }

}
