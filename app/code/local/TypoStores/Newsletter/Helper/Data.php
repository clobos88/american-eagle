<?php
/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
?>
<?php
class TypoStores_Newsletter_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCfgEnable()
    {
        return Mage::getStoreConfig('typostoresnewsletter/display_options/enable');
    }
    // public function getCfgWidth()
    // {
    //     return Mage::getStoreConfig('typostoresnewsletter/display_options/width');
    // }
    // public function getCfgHeight()
    // {
    //     return Mage::getStoreConfig('typostoresnewsletter/display_options/height');
    // }
    public function getCfgBackgroundColor()
    {
        return Mage::getStoreConfig('typostoresnewsletter/display_options/background_color');
    }
    public function getCfgNewsletterImage()
    {
        return Mage::getStoreConfig('typostoresnewsletter/display_options/newsletter_image');
    }
    public function getCfgGotoLink()
    {
        return Mage::getStoreConfig('typostoresnewsletter/display_options/goto_link');
    }
    public function getCfgTitleNewsletter()
    {
        return Mage::getStoreConfig('typostoresnewsletter/display_options/title_newsletter');
    }
    public function getCfgBackgroundImage()
    {
        return Mage::getStoreConfig('typostoresnewsletter/display_options/background_image');
    }
    public function getCfgIntro()
    {
        return Mage::getStoreConfig('typostoresnewsletter/display_options/intro');
    }
    public function getCfgBehaviourNewsletter()
    {
        return Mage::getStoreConfig('typostoresnewsletter/display_options/behaviour');
    }
    public function getCfgOnlyHomeNewsletter()
    {
        return Mage::getStoreConfig('typostoresnewsletter/display_options/show_only_home');
    }
}