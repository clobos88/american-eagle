window.Ymm = (function($, window, document, ajax_url) {
    "use strict";

    var $form = $('#ymm-search');
    var $go = $('#ymm-go');
    var $dropdowns = $('[data-dropdown]');
    var selected = { };
    var ajax = function(endpoint) {
        return $.ajax(ajax_url + endpoint, {
            type: 'POST',
            dataType: 'JSON',
            data: selected
        });
    };
    var clear = function() {
        for(var i = 0; i < arguments.length; i++) {
            var $dd = $('#' + arguments[i]);
            $dd.addClass('ymm-dropdown--disabled')
                .find('.ymm-dropdown__option--label')
                .find('.ymm-dropdown__selected')
                .text($dd.data('dropdownLabel'));
            $dd.find('.ymm-dropdown__options').empty();
            $form.find('[name="' + $dd.data('dropdown') + '"]').val('');
        }
    };
    var populate = function(id, items) {
        var $dd = $('#'+id);

        clear(id);

        for(var i = 0; i < items.length; i++) {
            $dd.find('.ymm-dropdown__options').append($('<div/>', {
                'text'  : items[i],
                'class' : 'ymm-dropdown__option'
            }));
        }

        if(items.length > 0) {
            $dd.removeClass('ymm-dropdown--disabled');
            $dd.removeClass('ymm-dropdown--loading');
            $dd.addClass('ymm-dropdown--active');
        }
    };
    var setValue = function(el, loadingEl) {
        var $el = $(el);
        var k = $el.data('dropdown');

        if(loadingEl) {
            $('#' + loadingEl).addClass('ymm-dropdown--loading');
        }

        $go.addClass('ymm-button--disabled');
        selected[k] = $el.find('.ymm-dropdown__selected').text();
        $form.find('[name="' + k + '"]').val(selected[k]);
    };

    $dropdowns.each(function() {
        var $dd = $(this);
        var $ddLabel = $dd.find('.ymm-dropdown__option--label');
        var $ddSelected = $ddLabel.find('.ymm-dropdown__selected');

        $ddSelected.text($dd.data('dropdownLabel'));

        $ddLabel.on('click', function() {
            if($dd.hasClass('ymm-dropdown--disabled')) {
                return false;
            }

            if($dd.hasClass('ymm-dropdown--active')) {
                $dd.removeClass('ymm-dropdown--active');
            } else {
                $dd.addClass('ymm-dropdown--active');
            }
        });

        $dd.on('click', '.ymm-dropdown__options .ymm-dropdown__option', function() {
            var $ddOption = $(this);
            $dd.find('.ymm-dropdown__options .ymm-dropdown__option').removeClass('ymm-dropdown__option--selected');
            $dd.removeClass('ymm-dropdown--active');

            $ddSelected.text($ddOption.text());
            $ddOption.addClass('ymm-dropdown__option--selected');
            $dd.trigger('change');
        });
    });

    $('#select-year').on('change', function() {
        setValue(this, 'select-make');
        clear('select-make', 'select-model');

        ajax('get_makes').success(function(response) {
            populate('select-make', response.items);
        });
    });

    $('#select-make').on('change', function() {
        setValue(this, 'select-model');
        clear('select-model');
        ajax('get_models').success(function(response) {
            populate('select-model', response.items);
        });
    });

    $('#select-model').on('change', function() {
        setValue(this);
        $go.removeClass('ymm-button--disabled');
    });

    $go.on('click', function(e) {
        if($(this).hasClass('ymm-button--disabled')) {
            e.preventDefault();
        }
    });

    $(document).on('click', function(e) {
        if (!$dropdowns.is(e.target) && $dropdowns.has(e.target).length === 0) {
            $dropdowns.removeClass('ymm-dropdown--active');
        }
    });

})(window.jQuery, window, document, window.ymmAjaxUrl || '');