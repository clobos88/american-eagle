/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
$typo(function ($) {
    $('body').append('<div id="notification"></div>');
    $(document).on('click', 'button.close', function(event) {
        event.preventDefault();
        $(this).parent().fadeOut('slow', function () {
            $(this).remove();
        });
    });
});
function showBox(message, wait) {
    $typo('#notification').html('<div class="success" style="display: none;">' +
    '' + message + '' +
    '<button class="close"><i aria-hidden="true" class="icon_close"></i></button></div>');
    $typo('.success').show();
    if (wait) {
        $typo('.success').addClass('wait-loading');
        $typo('.success .close').hide();
    } else {
        $typo('.success').removeClass('wait-loading');
        $typo('.success .close').show();
    }
    $typo('.success').fadeIn('slow');
    if (!wait) {
        setTimeout(function () {
            $typo('.success').delay(500).fadeOut(1000);
        }, 4000);
    }
}

function addtowishlist(el) {
    var url = jQuery(el).attr('href');
    var checkUpdateUrl = (url.indexOf('wishlist/index/updateItemOptions') > -1);
    var btn_wishlist = jQuery(el).find('i');
    if (checkUpdateUrl) {
        url = url.replace("wishlist/index/updateItemOptions", "ajaxcart/index/updateWishlist");
    } else {
        url = url.replace("wishlist/index/add", "ajaxcart/index/wishlist");
    }
    data = '&isAjax=1';
    // showBox(datatext.load, true);
    btn_wishlist.addClass('fa-spin');
    try {
        jQuery.ajax({
            url: url,
            dataType: 'jsonp',
            data: data,
            type: 'post',
            success: function (data) {
                btn_wishlist.removeClass('fa-spin');
                showBox(data.message, false);
                if (data.status != 'ERROR') {
                    jQuery('.header .links').replaceWith(data.toplink);
                }
                if (data.status != 'ERROR' && jQuery('.block-wishlist').length) {
                    jQuery('.block-wishlist').replaceWith(data.sidebar);
                }
            }
        });
    } catch (e) {
    }
}
function removetowishlist(el) {
    var url = jQuery(el).attr('href');
    data = '&isAjax=1';
    url = url.replace("wishlist/index", "ajaxcart/index");
    showBox(datatext.load, true);
    try {
        jQuery.ajax({
            url: url,
            dataType: 'jsonp',
            data: data,
            type: 'post',
            success: function (data) {
                showBox(data.message, false);
                if (data.status != 'ERROR') {
                    jQuery('.header .links').replaceWith(data.toplink);
                }
                if (data.status != 'ERROR' && jQuery('.block-wishlist').length) {
                    jQuery('.block-wishlist').replaceWith(data.sidebar);
                    if (data.sidebar == '') {
                        jQuery('.block-wishlist').remove();
                    }
                }
            }
        });
    } catch (e) {
    }
}
function addtocompare(el) {
    var url = jQuery(el).attr('href');
    var btn_compare = jQuery(el).find('i');
    data = '&isAjax=1';
    url = url.replace("catalog/product_compare/add", "ajaxcart/index/compare");
    // showBox(datatext.load, true);
    btn_compare.addClass('fa-spin');
    try {
        jQuery.ajax({
            url: url,
            dataType: 'jsonp',
            data: data,
            type: 'post',
            success: function (data) {
                btn_compare.removeClass('fa-spin');
                showBox(data.message, false);
                if (data.status != 'ERROR' && jQuery('.block-compare').length) {
                    jQuery('.block-compare').replaceWith(data.output);
                }
                if (data.status != 'ERROR') {
                    jQuery('.block-top-compare').html();
                    jQuery('.block-top-compare').html(data.topcompare);
                }
                jQuery(".compare-list .ajax-over").mCustomScrollbar({
                    theme:"dark-2",
                    scrollButtons:{
                      enable:true
                    }
                });
            }
        });
    } catch (e) {
    }
}
function removecompare(el) {
    var url = jQuery(el).attr('href');
    data = '&isAjax=1';
    url = url.replace("catalog/product_compare/remove", "ajaxcart/index/rmcompare");
    showBox(datatext.load, true);
    try {
        jQuery.ajax({
            url: url,
            dataType: 'jsonp',
            data: data,
            type: 'post',
            success: function (data) {
                showBox(data.message, false);
                if (data.status != 'ERROR' && jQuery('.block-compare').length) {
                    jQuery('.block-compare').replaceWith(data.output);
                }
                if (data.status != 'ERROR') {
                    jQuery('.block-top-compare').html();
                    jQuery('.block-top-compare').html(data.topcompare);
                }
                jQuery(".compare-list .ajax-over").mCustomScrollbar({
                    theme:"dark-2",
                    scrollButtons:{
                      enable:true
                    }
                });
            }
        });
    } catch (e) {
    }
}
function clearallcompare(el) {
    var url = jQuery(el).attr('href');
    data = '&isAjax=1';
    url = url.replace("catalog/product_compare/clear", "ajaxcart/index/clearall");
    showBox(datatext.load, true);
    try {
        jQuery.ajax({
            url: url,
            dataType: 'jsonp',
            data: data,
            type: 'post',
            success: function (data) {
                showBox(data.message, false);
                if (data.status != 'ERROR' && jQuery('.block-compare').length) {
                    jQuery('.block-compare').replaceWith(data.output);
                }
                if (data.status != 'ERROR') {
                    jQuery('.block-top-compare').html();
                    jQuery('.block-top-compare').html(data.topcompare);
                }
                jQuery(".compare-list .ajax-over").mCustomScrollbar({
                    theme:"dark-2",
                    scrollButtons:{
                      enable:true
                    }
                });
            }
        });
    } catch (e) {
    }
}