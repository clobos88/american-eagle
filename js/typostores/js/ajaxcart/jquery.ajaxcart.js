/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
$typo(function ($) {
    //themeResize();
    //$(window).resize(function () {
    //    themeResize();
    //});
    $(document).on('click', '.typo-maincart a', function(event) {
        var checkUrl = ($(this).attr('href').indexOf('checkout/cart/delete') > -1);
        if (checkUrl && $(this).attr('class') != 'deletecart' && $(this).attr('class').indexOf('btn-remove2') == -1) {
            deletecart($(this).attr('href'));
            return false;
        }
    });
    $(document).on('click', '.success button.close', function(event) {
        event.preventDefault();
        $(this).parent().fadeOut('slow', function () {
            $(this).remove();
        });
    });
    $(document).on('click', '.options-cart', function(event) {
        event.preventDefault();
        $.colorbox({
            iframe: true,
            href: this.href,
            opacity: 0.8,
            speed: 300,
            current: '{current} / {total}',
            close: '<i aria-hidden="true" class="icon_close"></i>',
            width: '100%',
            height: '100%',
            maxWidth: '930px',
            maxHeight: '100%',
            fixed: true,
            onOpen: function () {
                $('#cboxLoadingGraphic').addClass('box-loading');
            },
            onComplete: function () {
                setTimeout(function () {
                    $('#cboxLoadingGraphic').removeClass('box-loading');
                }, 3000);
            }
        });
    });
    $(document).on('click', '.show-options', function(event) {
        event.preventDefault();
        if ($('.btn-cart-mobile').length == 0) {
            $(this).next().trigger('click');
        } else {
            return window.location.href = $(this).attr('data-submit');
        }
    });

    if ($('.product-view').length > 0 && $('.option-file').length == 0 && $('.btn-cart-mobile').length == 0) {
        productAddToCartForm.submit = function (button, url) {
            if (this.validator.validate()) {
                var form = this.form;
                var oldUrl = form.action;
                if (url) {
                    form.action = url;
                }
                var e = null;
                if (!url) {
                    url = $('#product_addtocart_form').attr('action');
                }
                var checkWishlistUrl = (url.indexOf('wishlist/index/cart') > -1);

                url = url.replace("checkout/cart", "ajaxcart/index");

                var data = $('#product_addtocart_form').serialize();
                data += '&isAjax=1';
                try {
                    if (checkWishlistUrl) {
                        this.form.submit();
                    } else {
                        if ($('#qty').val() == 0) {
                            if ($('.error_qty').length == 0) {
                                $('<span/>').html(Translator.translate("The quantity not zero!"))
                                    .addClass('error_qty')
                                    .appendTo($('.add-to-cart'));
                            }
                        } else {
                            $('.error_qty').remove();
                            $("span.textrepuired").html('');
                            if (!$('.ajaxcart-index-options').length > 0) {
                                //showCartBox(datatext.load, true);
                            }
                            jQuery(button).find('> span').hide();
                            jQuery(button).append('<i class="icon_loading fa-spin"></i>');
                            jQuery(button).addClass('pointer-event');
                            $.ajax({
                                url: url,
                                dataType: 'json',
                                type: 'post',
                                data: data,
                                success: function (data) {
                                    if (url.indexOf('updateItemOptions') > -1) {
                                        window.history.back();
                                    }

                                    parent.setAjaxData(data, button);
                                    $.colorbox.close();
                                    if (button && button != 'undefined') {
                                        button.disabled = false;
                                    };
                                },
                                error: function () {
                                    jQuery(button).removeClass('pointer-event');
                                }
                            });
                        }
                    }
                } catch (e) {
                }
                this.form.action = oldUrl;
                if (e) {
                    throw e;
                }

            }
            return false;
        }.bind(productAddToCartForm);
    }
});
function themeResize() {
    var width = $typo(window).width();
    if (width <= 767) {
        $typo('body').find('.btn-cart').addClass('btn-cart-mobile');
        if ($typo('.product-quick-view').length > 0) {
            $typo('body').find('.btn-cart').removeClass('btn-cart-mobile');
        }
    } else {
        $typo('body').find('.btn-cart').removeClass('btn-cart-mobile');
    }
}
function setAjaxData(data, elm) {
    if (data.status == 'ERROR') {
        jQuery(elm).find('i').remove();
        jQuery(elm).find('> span').show();
        jQuery(elm).removeClass('pointer-event');
        showCartBox(data.message);
    } else {
        var time = 6;
        var url_view_cart = jQuery('.typo-cart-label').attr('href');
        var txt_view_cart = Translator.translate("View cart");  
        var btn_view_cart = '<a class="button btn-cart btn-view-cart" href="'+url_view_cart+'">'+txt_view_cart+' <span class="count" data-count="'+time+'">'+time+'</span></a>';

        $typo('.typo-maincart').html('');
        if ($typo('.typo-maincart')) {
            $typo('.typo-maincart').append(data.output);
        }
        if ($typo('.header .links')) {
            $typo('.header .links').replaceWith(data.toplink);
        }
        // $typo.colorbox.close();
        // showCartBox(data.message, false);
        jQuery(elm).find('.icon_loading').remove();
        jQuery(elm).append('<i aria-hidden="true" class="icon_check"></i>');
        jQuery('.typo-cart-label .print').addClass('animate-bounceIn active');
        
        setTimeout(function(){
            jQuery(elm).after(btn_view_cart);
            jQuery(elm).hide();

            var counter = jQuery(elm).find('+ .btn-view-cart span.count');
            var countHide = function(){
                var time = counter.attr('data-count');
                if (time<= 0 || time == 'undefined') {
                    $typo.colorbox.close();
                    return;
                }
                else time-=1;
                counter.html(time).attr('data-count', time);
                setTimeout(function(){ countHide() }, 1000);
            }
            
            countHide();
        }, 2000);


        setTimeout(function () {
            jQuery(elm).fadeIn('slow').removeClass('pointer-event');
            jQuery(elm).find('.icon_check').remove();
            jQuery(elm).find('> span').show();
            jQuery(elm).find('+ .btn-view-cart').remove();
        }, 6000);

        $typo("#ajaxcart-scrollbar").mCustomScrollbar({
            theme:"dark-2",
            scrollButtons:{
              enable:true
            }
        });
    }
}
function showCartBox(message, wait) {
    $typo('#notification').html('<div class="success" style="display: none;">' +
    '' + message + '' +
    '<button class="close"><i aria-hidden="true" class="icon_close"></i></button></div>');
    if (wait) {
        $typo('.success').addClass('wait-loading');
        $typo('.success .close').hide();
    } else {
        $typo('.success').removeClass('wait-loading');
        $typo('.success .close').show();
    }
    $typo('.success').show();
    if (!wait) {
        setTimeout(function () {
            $typo('.success').delay(500).fadeOut(1000);
        }, 4000);
    }
}
function setLocation(url, elm) {
    var checkUrl = (url.indexOf('checkout/cart') > -1);
    var pass = true;
    if ($typo('body').find('.btn-cart-mobile').length > 0) {
        pass = false;
    }
    if (checkUrl && pass) {
        var data = '&isAjax=1&qty=1';
        url = url.replace("checkout/cart", "ajaxcart/index");
        // showCartBox(datatext.load, true);
        jQuery(elm).find('> span').hide();
        jQuery(elm).append('<i class="icon_loading fa-spin"></i>');
        jQuery(elm).addClass('pointer-event');
        try {
            $typo.ajax({
                url: url,
                dataType: 'json',
                data: data,
                type: 'post',
                success: function (data) {
                    setAjaxData(data, elm);
                },
                error: function () {
                    jQuery(elm).removeClass('pointer-event');
                }
            });
        } catch (e) {
        }
        return false;
    }
    return window.location.href = url;
}
function deletecart(url) {
    var checkUrl = (url.indexOf('checkout/cart') > -1);
    if (checkUrl) {
        var data = '&isAjax=1&qty=1';
        var url = url.replace("checkout/cart", "ajaxcart/index");
        showCartBox(datatext.load, true);
        $typo.ajax({
            url: url,
            dataType: 'json',
            data: data,
            type: 'post',
            success: function (data) {
                setAjaxData_delete(data, false);
                if ($typo('.checkout-cart-index').length || $typo('.typostores-onestepcheckout-index-index').length || $typo('.checkout-onepage-index').length) {
                    window.location.reload();
                };
            }
        });
    }
    return false;
}

function setAjaxData_delete(data, iframe) {
    if (data.status == 'ERROR') {
        showCartBox(data.message);
    } else {
        $typo('.typo-maincart').html('');
        if ($typo('.typo-maincart')) {
            $typo('.typo-maincart').append(data.output);
        }
        if ($typo('.header .links')) {
            $typo('.header .links').replaceWith(data.toplink);
        }
        $typo.colorbox.close();
        showCartBox(data.message, false);
        $typo("#ajaxcart-scrollbar").mCustomScrollbar({
            theme:"dark-2",
            scrollButtons:{
              enable:true
            }
        });
    }
}