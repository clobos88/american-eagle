/**
 * ┌┬┐┬ ┬┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┌─┐┌─┐┌┬┐
 *  │ └┬┘├─┘│ │└─┐ │ │ │├┬┘├┤ └─┐ │  │ ││││
 *  ┴  ┴ ┴  └─┘└─┘ ┴ └─┘┴└─└─┘└─┘o└─┘└─┘┴ ┴
 *
 * @copyright    Copyright (C) 2015 typostores.com. All Rights Reserved.
 *
 */
;'use strict';

var TypoStoresFilter = Class.create();
TypoStoresFilter.prototype = {
    container: null,
    layer: null,
    name: null,
    initialize: function(name, config){
        this.name = name;
        this.config = config;
        if (this.config.enable){
            if (this.config.bar){
                NProgress.configure({
                    showSpinner: true,
                    easing: 'ease',
                    speed: 200,
                    minimum: 0.1,
                    trickleRate: 0.05,
                    trickleSpeed: 500
                });
            }
            document.observe('dom:loaded', function(){
                this.collect();
            }.bind(this));
        }
    },
    collect: function(){
        this.container = $$('.col-main')[0];
        this.layer = $$('.block-layered-nav')[0];
        this.initLinkFilter();
    },
    setConfig: function(obj){
        Object.extend(this.config, obj);
    },
    initLinkFilter: function(){
        if (this.container){
            this.container.select('a').each(function(a){
                $(a).observe('click', function(ev){
                    var a = Event.findElement(ev, 'a'),
                        URL = new URI(a.href);

                    if (URL.hasQuery('p') || URL.hasQuery('order') || URL.hasQuery('mode')){
                        Event.stop(ev);
                        this.sendRequest(a.href);
                    }
                }.bind(this));
            }, this);
        }

        if (this.layer){
            this.layer.select('a').each(function(a){
                $(a).observe('click', function(ev){
                    var a = Event.findElement(ev, 'a');
                    Event.stop(ev);
                    this.sendRequest(a.href);
                }.bind(this));
            }, this);
        }
    },
    initPriceFilter: function(obj){
        var slider      = $(obj.id),
            range       = $R(obj.min, obj.max),
            URL         = new URI(obj.url);

        jQuery(slider).slider({
            range: true,
            min: obj.min,
            max: obj.max,
            values: obj.values,
            slide: function( event, ui ) {
                jQuery("#layer-price-min").text(ui.values[0]);
                jQuery("#layer-price-max").text(ui.values[1]);
            },
            change: function( event, ui ) {
                var priceMin = Math.floor(ui.values[ 0 ]),
                    priceMax = Math.ceil(ui.values[ 1 ]);

                jQuery(slider).slider("disable");
                URL.setQuery('price', priceMin + '-' + priceMax);
                this.sendRequest(URL.toString(), function(){
                    jQuery(slider).slider("enable");
                });
                jQuery( "#amount" ).val( obj.symbol + ui.values[ 0 ] + " - " + obj.symbol + ui.values[ 1 ] );
            }.bind(this)
        });
        jQuery("#layer-price-min").text(jQuery(slider).slider( "values", 0 ));
        jQuery("#layer-price-max").text(jQuery(slider).slider( "values", 1 ));
    },
    getParams: function(){
        return {
            isAjax: true
        };
    },
    setAjaxLocation: function(url, elm){
        var url = arguments[0],
            URL = new URI(url);

        if (URL.hasQuery('limit') || URL.hasQuery('order')){
            this.sendRequest(url);
        }else setLocation(url, elm);
    },
    sendRequest: function(url, success, error){
        if (this.config.enable){
            if (this.config.bar) NProgress.start();
            if (typeof window.history.pushState == 'function') {
                var stateObj = { path: url };
                // window.history.pushState(stateObj, null, url);
                history.replaceState(stateObj, null, url);

                window.onpopstate = function(event) {
                    if(event && event.state) {
                        location.reload();
                    }
                };
            }
            new Ajax.Request(url, {
                parameters: this.getParams(),
                onSuccess: function(transport){
                    try{
                        var response = transport.responseText.evalJSON(),
                            main = response.main ? response.main.replace(/setLocation/g, this.name+'.setAjaxLocation') : null,
                            layer = response.layer || null;

                        if (main && this.container) this.container.update(main);
                        if (layer && this.layer) this.layer.replace(layer);
                        //setGridItem($typo);
                        setTimeout(function(){
                            this.collect();
                        }.bind(this));
                        if (success) success(transport);
                    }catch(e){
                        console.log(e.message);
                    }
                }.bind(this),
                onFailure: function(transport){
                    if (error) error(transport);
                },
                onComplete: function(){
                    if (this.config.bar) NProgress.done();
                    ConfigurableMediaImages.ajaxLoadSwatchList();
                }.bind(this)
            });
        }else{
            setLocation(url);
        }
    }
};